-- Helper functions
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local wibox = require("wibox")


local helpers = {}

helpers.rounded_rect = function(radius)
    return function(cr, width, height)
        gears.shape.rounded_rect(cr, width, height, radius)
    end
end

helpers.update_bar = function(widget, value)
    widget.value = value
    widget.color = beautiful.fg_focus
end

helpers.set_text = function(widget, text, color)
    color = color or beautiful.fg_focus
    widget.text = text
    widget.markup = "<span foreground='".. color .."'>".. widget.text .."</span>"
end

helpers.prefix_widget = function(text, color)
    color = color or beautiful.fg_focus
    local widget =  wibox.widget.textbox(text)
    widget.markup = "<span foreground='".. color .."'>".. widget.text .."</span>"
    return widget
end

function helpers.arc_widget()
    return wibox.widget {
        forced_width = beautiful.wibar_height - 8,
        forced_height = beautiful.wibar_height - 8,
        thickness = 3,
        bg = beautiful.bg_focus,
        colors = {beautiful.fg_focus},
        min_value = 0,
        max_value = 1,
        value = 0.33,
        -- paddings = {top = 4, bottom = 4, left = 4, right = 4},
        rounded_edge = true,
        -- margins = {top = 7, bottom = 7},
        widget = wibox.container.arcchart
    }
end

function helpers.bar_widget()
    return wibox.widget {
        forced_width = 100,
        paddings = 0,
        border_width = 2,
        color = beautiful.fg_focus,
        background_color = beautiful.bg_focus,
        shape = gears.shape.rounded_bar,
        bar_shape = gears.shape.rounded_bar,
        clip = true,
        margins = { top = 4, bottom = 4},
        widget = wibox.widget.progressbar
    }
end

function helpers.client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end

return helpers;