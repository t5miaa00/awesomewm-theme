local awful = require("awful")
local gears = require("gears")
local spawn = require("awful.spawn")
local watch = require("awful.widget.watch")
local naughty = require("naughty")
local wibox = require("wibox")
local helpers = require("helpers")
local beautiful = require("beautiful")

-- local volume = awful.util.get_configuration_dir() .. "getvolume.sh"
local checker = "amixer -D pulse sget Master"
local icon_font = "material icons 12"

local bar_prefix = wibox.widget.textbox("")
bar_prefix.font = icon_font

local volumebar = helpers.bar_widget()

local volumebar_widget = wibox.widget {
    bar_prefix,
    volumebar,
    layout = wibox.layout.fixed.horizontal,
    spacing = 10
}

local update_bar = function(widget, stdout, _, _, _)
    -- local volume = stdout
    local mute = string.match(stdout, "%[(o%D%D?)%]")
    local volume = string.match(stdout, "(%d?%d?%d)%%")
    volume = tonumber(string.format("% 3d", volume))
    volumebar_widget:set_opacity(1)
    widget.value = volume / 100
    helpers.set_text(bar_prefix, "")

    if mute == "off" then
        -- widget.color = "#000000"
        volumebar_widget:set_opacity(0.3)
        helpers.set_text(bar_prefix, "")
    end
end

volumebar_widget:connect_signal("button::press", function(_,_,_,button)
    if (button == 4)     then awful.spawn("amixer -D pulse sset Master 5%+", false)
    elseif (button == 5) then awful.spawn("amixer -D pulse sset Master 5%-", false)
    elseif (button == 1) then awful.spawn("amixer -D pulse sset Master toggle", false)
    end

    spawn.easy_async(checker, function(stdout, stderr, exitreason, exitcode)
        update_bar(volumebar, stdout, stderr, exitreason, exitcode)
    end)
end)

watch(checker, 10, update_bar, volumebar)

return volumebar_widget