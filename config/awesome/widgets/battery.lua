local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local wibox = require("wibox")
local beautiful = require("beautiful")
local helpers = require("helpers")

local capacity = "cat /sys/class/power_supply/BAT1/capacity"
local status = "cat /sys/class/power_supply/BAT1/status"
local icon_font = "material icons 12"

local battery = helpers.arc_widget()
local bat_capacity = wibox.widget.textbox("bat")
bat_capacity.font = "fira sans 9"
bat_capacity.align = "center"

local charging_indicator = wibox.widget.textbox("")
charging_indicator.color = beautiful.fg_focus
charging_indicator.align = "center"
charging_indicator.font = icon_font
charging_indicator.visible = false

local battery_widget = wibox.widget {
    charging_indicator,
    {
        battery,
        bat_capacity,
        layout = wibox.layout.stack,
    },
    spacing = 0,
    layout = wibox.layout.fixed.horizontal,
}

function battery_exists()
    awful.spawn.easy_async(status, function(_,_,_, code)
        if code == 0 then
            return true
        else
            return false
        end
    end)
end

function update_capacity(widget, stdout, _, _, _)
    local value = tonumber(stdout, 10)
    value = value / 100
    helpers.update_bar(widget, value)

    awful.spawn.easy_async(status, function(out, err, reason, code)
        local data = {status = out, capacity = math.floor(value * 100)}
        update_status(bat_capacity, data, err, reason, code)
    end)
end

function update_status(widget, stdout, _, _, _)
    local string = "ERR"
    if stdout.status == "Charging\n" then
        string = "CHR"
        charging_indicator.visible = true
    elseif stdout.status == "Full\n" then
        string = "FULL"
        charging_indicator.visible = true
    else
        string = "BAT"
        charging_indicator.visible = false
    end
    helpers.set_text(bat_capacity, stdout.capacity)
end

-- awful.widget.watch(status,   5, update_status,   prefix)
if battery_exists() then
    awful.widget.watch(capacity, 10, update_capacity, bat_capacity)
    return battery_widget
else
    return wibox.widget.base.empty_widget()
end