#! /usr/bin/env bash

function run_once {
    if ! prep $1; then
        $@&
    fi
}

run_once compton