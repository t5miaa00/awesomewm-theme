local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local helpers = require("helpers")

local titlebars = {}

-- Mouse Buttons
titlebars.buttons = gears.table.join(
    -- Left: Move
    awful.button({ }, 1, function()
        c = mouse.object_under_pointer()
        client.focus = c
        c:raise()
        awful.mouse.client.move(c)
    end),
    -- Middle: Kill
    awful.button({ }, 2, function()
        c = mouse.object_under_pointer()
        c:kill()
    end),
    -- Right: resize
    awful.button({ }, 3, function()
        c = mouse.object_under_pointer()
        client.focus = c
        c:raise()
        awful.mouse.client.resize(c)
    end)
)

client.connect_signal("manage", function(c, startup)
    if not c.fullscreen then
        c.shape = helpers.rounded_rect(10)
    end
end)

awful.titlebar.enable_tooltip = false

client.connect_signal("request::titlebars", function(c)
    local buttons = titlebars.buttons
end)

return titlebars