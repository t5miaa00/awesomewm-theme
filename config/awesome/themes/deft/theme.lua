---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gears = require("gears")
local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
local theme_path = os.getenv("HOME") .. "/.config/awesome/themes/deft/"
local icon_path = theme_path .. "icons/"

local helpers = require("helpers")

local theme = {}

theme.font          = "Fira Code 12"
theme.taglist_font  = theme.font
theme.icon_type     = "-thin"
theme.icon_ext      = "svg"

theme.bg_focus     = "#222d32"
theme.bg_normal      = "#263238"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#263238"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#00BCD4"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = 5
theme.border_width  = dpi(0)
theme.border_normal = "#000000"
theme.border_focus  = "#535d6c"
theme.border_marked = "#91231c"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"
theme.menu_height = 32
theme.menu_width = dpi(200)
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

theme.titlebar_height = dpi(32)
theme.titlebar_padding = 10
theme.wibar_height = theme.titlebar_height
theme.wibar_bg = "#00000054"

-- Define the image to load
theme.titlebar_close_button_normal = icon_path .. "close".. theme.icon_type .."." .. theme.icon_ext
theme.titlebar_close_button_focus  = icon_path .. "close".. theme.icon_type .."." .. theme.icon_ext

theme.titlebar_minimize_button_normal = icon_path .. "min".. theme.icon_type .."." .. theme.icon_ext
theme.titlebar_minimize_button_focus  = icon_path .. "min".. theme.icon_type .."." .. theme.icon_ext

-- theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
-- theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
-- theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
-- theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

-- theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
-- theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
-- theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
-- theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

-- theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
-- theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
-- theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
-- theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

theme.menu_opacity = 0.33
theme.titlebar_maximized_button_normal_inactive = icon_path .. "full".. theme.icon_type .."." .. theme.icon_ext
theme.titlebar_maximized_button_focus_inactive  = icon_path .. "full".. theme.icon_type .."." .. theme.icon_ext
theme.titlebar_maximized_button_normal_active =   icon_path .. "win".. theme.icon_type .."." .. theme.icon_ext
theme.titlebar_maximized_button_focus_active  =   icon_path .. "win".. theme.icon_type .."." .. theme.icon_ext

-- theme.wallpaper = "/home/aatu/Kuvat/taustakuvat/5a675f0cf31ea5f5f38895638c30094f.jpg"
theme.wallpaper = "/home/aatu/Kuvat/taustakuvat/cnhk7aib5rpz.png"
theme.layout_floating = icon_path .. "win".. theme.icon_type .."." .. theme.icon_ext
theme.layout_max =      icon_path .. "full".. theme.icon_type .."." .. theme.icon_ext
theme.layout_tile =     icon_path .. "tile".. theme.icon_type .."." .. theme.icon_ext
-- You can use your own layout icons like this:
-- theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
-- theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
-- theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
-- theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
-- theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
-- theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
-- theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
-- theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
-- theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
-- theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
-- theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
-- theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
-- theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.titlebar_height, theme.fg_focus, theme.bg_normal
)

theme = theme_assets.recolor_titlebar(
    theme, theme.fg_normal, "normal", nil
)
theme = theme_assets.recolor_titlebar(
    theme, theme.fg_normal, "focus", "hover"
)
theme = theme_assets.recolor_titlebar(
    theme, theme.fg_focus, "focus", nil
)
theme = theme_assets.recolor_layout(
    theme, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "Papirus"

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
